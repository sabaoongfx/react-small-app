//npm install axios


import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';

function App() {
  const [quote, setQuote] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetchQuote();
  }, []);

  const fetchQuote = async () => {
    try {
      const response = await axios.get(
        'https://ron-swanson-quotes.herokuapp.com/v2/quotes'
      );
      setQuote(response.data[0]);
      setLoading(false);
    } catch (error) {
      console.error('Error fetching quote:', error);
      setLoading(false);
    }
  };

  const handleGetQuote = () => {
    fetchQuote();
  };

  return (
    <div className="App">
      <h1>Ron Swanson Quotes</h1>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <>
          <p className="quote-text">"{quote}"</p>
          <button onClick={handleGetQuote}>Get Another Quote</button>
        </>
      )}
    </div>
  );
}

export default App;
