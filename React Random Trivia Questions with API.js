//npm install axios

import React, { useState, useEffect } from 'react';
import axios from 'axios';

function App() {
  const [question, setQuestion] = useState('');
  const [answer, setAnswer] = useState('');
  const [loading, setLoading] = useState(true);
  const [showAnswer, setShowAnswer] = useState(false);

  useEffect(() => {
    fetchRandomTrivia();
  }, []);

  const fetchRandomTrivia = async () => {
    try {
      const response = await axios.get('http://jservice.io/api/random');
      const { question, answer } = response.data[0];
      setQuestion(question);
      setAnswer(answer);
      setLoading(false);
      setShowAnswer(false); // Reset answer visibility
    } catch (error) {
      console.error('Error fetching trivia:', error);
      setLoading(false);
    }
  };

  const handleGetTrivia = () => {
    fetchRandomTrivia();
  };

  const handleShowAnswer = () => {
    setShowAnswer(true);
  };

  return (
    <div className="App">
      <h1>Random Trivia</h1>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <>
          <p className="question">{question}</p>
          {showAnswer && <p className="answer">Answer: {answer}</p>}
          <button onClick={handleShowAnswer}>Show Answer</button>
          <button onClick={handleGetTrivia}>Get Another Trivia</button>
        </>
      )}
    </div>
  );
}

export default App;
