import React, { useState, useEffect } from 'react';

function App() {
  const [quote, setQuote] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetchRandomQuote();
  }, []);

  const fetchRandomQuote = () => {
    setLoading(true);

    // Simulate fetching a random quote (you can replace this with a real API call)
    setTimeout(() => {
      const quotes = [
        "The only way to do great work is to love what you do. - Steve Jobs",
        "Innovation distinguishes between a leader and a follower. - Steve Jobs",
        "Your time is limited, don't waste it living someone else's life. - Steve Jobs",
        "Life is what happens when you're busy making other plans. - John Lennon",
        "The only thing we have to fear is fear itself. - Franklin D. Roosevelt",
      ];

      const randomIndex = Math.floor(Math.random() * quotes.length);
      setQuote(quotes[randomIndex]);
      setLoading(false);
    }, 1000);
  };

  return (
    <div className="App">
      <h1>Random Quote Generator</h1>
      <button onClick={fetchRandomQuote}>Get Random Quote</button>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <div className="quote">
          <p>{quote}</p>
        </div>
      )}
    </div>
  );
}

export default App;
