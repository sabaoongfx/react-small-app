# In this project, I have added multiple React Small Apps

## Getting started


1. Clone this repository to your local machine.

2. Navigate to the project directory.

3. Install dependencies by running:

   ```bash
   npm install

   Start the development server:
   npm start

   //for some apps you need to install axios
   npm install axios


# License

This project is licensed under the MIT License. 

# Author
Sabaoon Ullah

Feel free to reach out if you have any questions or suggestions!
