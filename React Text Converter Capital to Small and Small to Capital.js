import React, { useState } from 'react';

function App() {
  const [inputText, setInputText] = useState('');
  const [outputText, setOutputText] = useState('');

  const handleInputChange = (e) => {
    setInputText(e.target.value);
  };

  const convertToLowerCase = () => {
    setOutputText(inputText.toLowerCase());
  };

  const convertToUpperCase = () => {
    setOutputText(inputText.toUpperCase());
  };

  return (
    <div className="App">
      <h1>Text Case Converter</h1>
      <input
        type="text"
        placeholder="Enter text..."
        value={inputText}
        onChange={handleInputChange}
      />
      <div>
        <button onClick={convertToLowerCase}>Convert to Lowercase</button>
        <button onClick={convertToUpperCase}>Convert to Uppercase</button>
      </div>
      <div>
        <p>Result: {outputText}</p>
      </div>
    </div>
  );
}

export default App;
